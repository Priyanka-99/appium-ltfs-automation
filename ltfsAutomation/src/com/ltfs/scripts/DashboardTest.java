package com.ltfs.scripts;

import org.junit.Test;
import org.junit.jupiter.api.Order;

import com.ltfs.generics.BaseLib;

import io.appium.java_client.MobileElement;

public class DashboardTest extends BaseLib{

	@Test
	@Order(1)
	public void tc1() throws InterruptedException {
		//tap on logout icon
		MobileElement dashboardEle=(MobileElement) driver.findElementById("com.ltfs.lti.ltfs:id/bb_bottom_bar_icon");
		dashboardEle.click();	
		Thread.sleep(3000);
	}

	@Test
	@Order(2)
	public void tc2() throws InterruptedException {
		//tap on dashboard icon
		MobileElement logoutEle=(MobileElement)driver.findElementById("com.ltfs.lti.ltfs:id/item1");
		logoutEle.click();
		//tap on yes button
		MobileElement acceptBtn = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v7.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[1]");
		acceptBtn.click();
		Thread.sleep(3000);
	}


	@Test
	@Order(3) 
	public void tc3() throws InterruptedException {
		//tap on new
		MobileElement newEle=(MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.ImageView");
		newEle.click();
		Thread.sleep(3000);
	}
	
	@Test
	@Order(4) 
	public void tc4() throws InterruptedException {
		MobileElement refEle=(MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView");
		refEle.click();	
		Thread.sleep(3000);
	}
	
	@Test
	@Order(5) 
	public void tc5() throws InterruptedException {
		MobileElement profileEle=(MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.TextView");
		profileEle.click();
		Thread.sleep(3000);
	}
	
	@Test
	@Order(6) 
	public void tc6() throws InterruptedException {
		MobileElement searchEle=(MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView");
		searchEle.click();
		Thread.sleep(3000);
	}

}
