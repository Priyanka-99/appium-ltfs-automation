package com.ltfs.scripts;

import org.junit.Test;
import com.ltfs.generics.BaseLib;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;

public class ApplicantDetailsTest extends BaseLib {

	@Test
	public void tc1() throws InterruptedException {
		//script with positive inputs
		MobileElement aadharEle = (MobileElement) driver.findElementById("com.ltfs.lti.ltfs:id/act_aadhar");
		aadharEle.sendKeys("743189225154");

		MobileElement firstName = (MobileElement) driver.findElementById("com.ltfs.lti.ltfs:id/act_first_name");
		firstName.sendKeys("Gopal");

		MobileElement lastName = (MobileElement) driver.findElementById("com.ltfs.lti.ltfs:id/act_last_name");
		lastName.sendKeys("Ghosh");

		MobileElement fatherName = (MobileElement) driver
				.findElementById("com.ltfs.lti.ltfs:id/act_father_spouse_name");
		fatherName.sendKeys("Vallabh");

		MobileElement bankDetails = (MobileElement) driver.findElementById("com.ltfs.lti.ltfs:id/radio_account_no");
		bankDetails.click();

		Thread.sleep(2000);

		// script to scroll
		  driver.findElementByAndroidUIAutomator( "new UiScrollable(" +
		  "new UiSelector().scrollable(true)).scrollIntoView(" +
		  "new UiSelector().textContains(\"Gender*\"));").click();
		
		//new TouchAction(driver).press(PointOption.point(550, 640)).waitAction().moveTo(PointOption.point(550, 60)).release().perform();

		Thread.sleep(2000);

		MobileElement genderDrpdwn=(MobileElement) driver.findElementById("com.ltfs.lti.ltfs:id/spinner_gender");
		genderDrpdwn.click();

		Thread.sleep(2000);
	}


	/*
	 * @Test public void tc2() throws InterruptedException { //script with negative
	 * inputs: adding space between aadhar card no. MobileElement aadharEle =
	 * (MobileElement) driver.findElementById("com.ltfs.lti.ltfs:id/act_aadhar");
	 * aadharEle.sendKeys("7431 7922 5154");
	 * 
	 * Thread.sleep(2000);
	 * 
	 * MobileElement firstName = (MobileElement)
	 * driver.findElementById("com.ltfs.lti.ltfs:id/act_first_name");
	 * firstName.sendKeys("Dhruva");
	 * 
	 * MobileElement lastName = (MobileElement)
	 * driver.findElementById("com.ltfs.lti.ltfs:id/act_last_name");
	 * lastName.sendKeys("Shankar");
	 * 
	 * MobileElement fatherName = (MobileElement) driver
	 * .findElementById("com.ltfs.lti.ltfs:id/act_father_spouse_name");
	 * fatherName.sendKeys("Gopi");
	 * 
	 * MobileElement bankDetails = (MobileElement)
	 * driver.findElementById("com.ltfs.lti.ltfs:id/radio_account_no");
	 * bankDetails.click();
	 * 
	 * Thread.sleep(2000);
	 * 
	 * // script to scroll MobileElement ml = (MobileElement) new
	 * TouchAction(driver).press(PointOption.point(550,
	 * 640)).waitAction().moveTo(PointOption.point(550, 60)).release().perform();
	 * 
	 * Thread.sleep(2000); }
	 */

	/* @Test public void tc3() throws InterruptedException { //script with negative
	 * inputs: adding mobile no which is not acceptable MobileElement aadharEle =
	 * (MobileElement) driver.findElementById("com.ltfs.lti.ltfs:id/act_aadhar");
	 * aadharEle.sendKeys("7431 7922 5154");
	 * 
	 * Thread.sleep(2000);
	 * 
	 * MobileElement firstName = (MobileElement)
	 * driver.findElementById("com.ltfs.lti.ltfs:id/act_first_name");
	 * firstName.sendKeys("Raghu");
	 * 
	 * MobileElement lastName = (MobileElement)
	 * driver.findElementById("com.ltfs.lti.ltfs:id/act_last_name");
	 * lastName.sendKeys("Kopati");
	 * 
	 * MobileElement fatherName = (MobileElement) driver
	 * .findElementById("com.ltfs.lti.ltfs:id/act_father_spouse_name");
	 * fatherName.sendKeys("Janga");
	 * 
	 * MobileElement bankDetails = (MobileElement)
	 * driver.findElementById("com.ltfs.lti.ltfs:id/radio_account_no");
	 * bankDetails.click();
	 * 
	 * Thread.sleep(2000);
	 * 
	 * // script to scroll MobileElement ml = (MobileElement)
	 * driver.findElementByAndroidUIAutomator( "new UiScrollable(" +
	 * "new UiSelector().scrollable(true)).scrollIntoView(" +
	 * "new UiSelector().textContains(\"Gender*\"));"); ml.click();
	 * 
	 * Thread.sleep(2000);
	 * 
	 * try { MobileElement mobileNum=(MobileElement)
	 * driver.findElementById("com.ltfs.lti.ltfs:id/act_mobile_number");
	 * mobileNum.sendKeys("9310297106"); } catch(Exception e) { e.printStackTrace();
	 * } }
	 * 
	 * @Test public void tc4() throws InterruptedException { //script with negative
	 * inputs: adding pincode resulting in app crash MobileElement aadharEle =
	 * (MobileElement) driver.findElementById("com.ltfs.lti.ltfs:id/act_aadhar");
	 * aadharEle.sendKeys("743179225154");
	 * 
	 * Thread.sleep(2000);
	 * 
	 * MobileElement firstName = (MobileElement)
	 * driver.findElementById("com.ltfs.lti.ltfs:id/act_first_name");
	 * firstName.sendKeys("Vikruth");
	 * 
	 * MobileElement lastName = (MobileElement)
	 * driver.findElementById("com.ltfs.lti.ltfs:id/act_last_name");
	 * lastName.sendKeys("Singh");
	 * 
	 * MobileElement fatherName = (MobileElement) driver
	 * .findElementById("com.ltfs.lti.ltfs:id/act_father_spouse_name");
	 * fatherName.sendKeys("Jagadesh");
	 * 
	 * MobileElement bankDetails = (MobileElement)
	 * driver.findElementById("com.ltfs.lti.ltfs:id/radio_account_no");
	 * bankDetails.click();
	 * 
	 * Thread.sleep(2000);
	 * 
	 * // script to scroll MobileElement ml = (MobileElement)
	 * driver.findElementByAndroidUIAutomator( "new UiScrollable(" +
	 * "new UiSelector().scrollable(true)).scrollIntoView(" +
	 * "new UiSelector().textContains(\"Gender*\"));"); ml.click();
	 * 
	 * MobileElement addLine1=(MobileElement)
	 * driver.findElementById("com.ltfs.lti.ltfs:id/act_address_line_one");
	 * addLine1.sendKeys("334, Tower C");
	 * 
	 * MobileElement addLine2=(MobileElement)
	 * driver.findElementById("com.ltfs.lti.ltfs:id/act_address_line_two");
	 * addLine2.sendKeys("Suncity");
	 * 
	 * try { MobileElement pinCode=(MobileElement)
	 * driver.findElementById("com.ltfs.lti.ltfs:id/act_pincode");
	 * pinCode.sendKeys("400080"); } catch(Exception e) { e.printStackTrace(); } }
	 * 
	 */}
