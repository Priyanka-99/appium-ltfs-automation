package com.ltfs.generics;

import java.net.MalformedURLException;
import java.net.URL;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class BaseLib {

	public static AndroidDriver<MobileElement> driver;

	@BeforeMethod
	@Parameters({ "appPackage", "appActivity" })
	public void setUp(String appPackage, String appActivity) throws MalformedURLException, InterruptedException {

		URL url = new URL("http://127.0.0.1:4723/wd/hub");

		// Set up desired capabilities and pass the Android app-activity and app-package
		// to Appium
		DesiredCapabilities dc = new DesiredCapabilities();
		dc.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
		dc.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		// dc.setCapability(MobileCapabilityType.APP,
		// "C:\\apkfiles\\Farm_15_01_2021_10_39.apk");
		dc.setCapability("appPackage", appPackage);
		dc.setCapability("appActivity", appActivity);
		// dc.setCapability("appActivity","com.ltfs.lti.ltfs.ManualAadharDetails");
		// dc.setCapability("appActivity","com.ltfs.lti.fm.FmDashboardActivity");
		// dc.setCapability("appActivity","com.ltfs.lti.fm.FmSelectDealer");
		dc.setCapability("noReset", true);
		dc.setCapability("newCommandTimeout", 6000);
		// dc.setCapability("uiautomator2ServerLaunchTimeout", 90000);

		driver = new AndroidDriver<MobileElement>(url, dc);
		Thread.sleep(3000);
	}


	@AfterMethod
	public void teardown() { 
		// close the app 
		driver.quit(); 
	}


}
