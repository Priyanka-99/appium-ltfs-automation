package com.ltfs.pageobjects;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LoginPage {

	private AndroidDriver<MobileElement> driver;

	public LoginPage(AndroidDriver<MobileElement> driver) {
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/psnumber")
	private MobileElement userName; 

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/password")
	private MobileElement pswd;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/email_sign_in_button")
	private MobileElement loginBtn;


	//getters
	public MobileElement getUserName() {
		return userName;
	}

	public MobileElement getPswd() {
		return pswd;
	}

	public MobileElement getLoginBtn() {
		return loginBtn;
	}


}
