package com.ltfs.generics;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class ExcelGenerate {
	
	public static void main(String[]args) throws ParserConfigurationException, IOException, SAXException{
		
	    ExcelReportGenerator.generateExcelReport("ResultSheet.xls", "./test-output");

	}

}
