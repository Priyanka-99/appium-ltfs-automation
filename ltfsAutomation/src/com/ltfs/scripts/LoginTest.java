package com.ltfs.scripts;

import org.testng.annotations.Test;

import com.ltfs.generics.BaseLib;
import com.ltfs.pageobjects.LoginPage;

public class LoginTest extends BaseLib{
	
	@Test(priority=1)
	public void testLogin1() {
		
		LoginPage lp=new LoginPage(driver);
		lp.getUserName().sendKeys("VEN02735");
		lp.getPswd().sendKeys("welcome@1234");
		lp.getLoginBtn().click();
	
	System.out.println("Login test 1");
	}
	
	@Test(priority=2)
	public void testLogin2() {
		
		LoginPage lp=new LoginPage(driver);
		lp.getUserName().sendKeys("");
		lp.getPswd().sendKeys("welcome@1234");
		lp.getLoginBtn().click();
	
	System.out.println("Login test 2");
	}

}
